<%-- menu --%>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="header">MENU UTAMA</li>
    <li class="treeview">
        <a href="#">
            <i class="glyphicon glyphicon-asterisk"></i> <span>Menu</span> <i class="glyphicon glyphicon-menu-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="<%= path %>/sites/penduduk"><i class="glyphicon glyphicon-leaf"></i> Data Penduduk</a></li>
            <li><a href="<%= path %>/sites/jenis_bayar"><i class="glyphicon glyphicon-leaf"></i> Jenis Pembayaran</a></li>
        </ul>
    </li>

    <li class="header">PENGATURAN</li>
    <li><a href="#"><i class="glyphicon glyphicon-leaf"></i> Keluar</a></li>
</ul>
<%-- akhir menu --%>
