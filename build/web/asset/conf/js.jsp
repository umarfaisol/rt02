
<!-- jQuery 2.1.3 -->
<script src="<%= path %>/asset/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- jQueryUI 1.10.3 -->
<script src="<%= path %>/asset/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<%= path %>/asset/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<%= path %>/asset/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='<%= path %>/asset/plugins/fastclick/fastclick.min.js'></script>
<!-- DatePicker -->
<script src='<%= path %>/asset/plugins/datepicker/bootstrap-datepicker.js'></script>
<!-- AdminLTE App -->
<script src="<%= path %>/asset/dist/js/app.min.js" type="text/javascript"></script>
