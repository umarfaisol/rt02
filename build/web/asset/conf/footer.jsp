<%--
    Document   : footer
    Created on : Apr 30, 2016, 11:32:02 PM
    Author     : java
--%>

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0
  </div>
  <strong>Copyright &copy; 2016 <a href="http://www.gudangsourcecode.com">Umar Faisol</a>.</strong> All rights reserved.
</footer>
