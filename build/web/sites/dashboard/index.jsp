<%
final Penduduk p = new Penduduk(sqlitedb, request, response);
final String[] domisili = { "KTP GPR (" + p.getKtpGpr() + ")", "Domisili (" + p.getKtpDomisili() + ")" };

DatasetProducer pieDataDomisili = new DatasetProducer() {
    public Object produceDataset(Map params) {
        /*final String[] categories = { "apples", "pies", "bananas", "oranges" };*/
        DefaultPieDataset ds = new DefaultPieDataset();
        /*
        for (int i = 0; i < categories.length; i++) {
            int y = (int) (Math.random() * 10 + 1);
            ds.setValue(categories[i], new Integer(y));
        }
        */
        ds.setValue(domisili[0], new Integer(p.getKtpGpr()));
        ds.setValue(domisili[1], new Integer(p.getKtpDomisili()));

        return ds;
    }
    public String getProducerId() {
        return "PieDataProducer";
    }
    public boolean hasExpired(Map params, Date since) {
        return false;
    }
};
pageContext.setAttribute("pieDataDomisili", pieDataDomisili);
%>
<jsp:useBean id="pieEnhancer" class="de.laures.cewolf.cpp.PieEnhancer" />

<div class="row">
    <div class="col-md-6">

        <!-- AREA CHART -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Area Chart</h3>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="revenue-chart" style="height: 250px;"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- DONUT CHART -->
              <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Donut Chart</h3>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="sales-chart" style="height: 250px; position: relative;"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    </div>

    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Domisili</h3>
            </div>
            <div class="box-body chart-responsive">
                <cewolf:chart id="pie3d" title="Domisili" type="pie3D" showlegend="false">
                    <cewolf:data>
                        <cewolf:producer id="pieDataDomisili" />
                    </cewolf:data>
                </cewolf:chart>
                <cewolf:img chartid="pie3d" renderer="/cewolf" width="350" height="250"   >
                    <cewolf:map tooltipgeneratorid="pieToolTipGenerator"/>
                </cewolf:img>
                <cewolf:legend id="pie3d" renderer="/cewolf" width="450" height="40" />
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
