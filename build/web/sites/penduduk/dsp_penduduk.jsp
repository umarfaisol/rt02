<script type="text/javascript">
var pindah = function() {
    var ktp = document.getElementById("status_ktp");
    var status_ktp = ktp.options[ktp.selectedIndex].value;

    window.location = "<%= path %>/sites/penduduk/?status_ktp=" + status_ktp;
};
</script>
<%
Penduduk p = new Penduduk(db, request, response);
String status_ktp = request.getParameter("status_ktp");
if (status_ktp == null) {
    status_ktp = "2";
}
%>

<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="glyphicon glyphicon-minus"></i></button>
        </div>
    </div>

    <%
    String tambah = path + "/sites/penduduk/?action=tambah";
    %>
    <%-- menu --%>
    <div style="float: left; padding-left: 12px;">
        <a href="<%= tambah %>" class="btn btn-success" title="Tambah"><span class="glyphicon glyphicon-plus"></span></a>
    </div>

    <div style="float: right; padding-right: 12px;">
        <select class="form-control" id="status_ktp" name="status_ktp" style="width:130px;" onchange="pindah();">
            <option value="0"<%= (status_ktp.equals("0")?" selected=\"selected\"":"") %>>Domisili</option>
            <option value="1"<%= (status_ktp.equals("1")?" selected=\"selected\"":"") %>>KTP GPR</option>
            <option value="2"<%= (status_ktp.equals("2")?" selected=\"selected\"":"") %>>Semua</option>
        </select>
    </div>

    <div class="box-body">
        <table class="table table-bordered table-stripped table-condensed">
            <thead style="background-color: #B5B8B4;">
                <th style="width: 5%;">NO</th>
                <th>NIK</th>
                <th style="width: 35%;">Nama Lengkap</th>
                <th style="width: 15%;">Blok</th>
                <th style="width: 10%;">KTP</th>
                <th style="width: 10%;">Opsi</th>
            </thead>
            <%-- data penduduk --%>
            <%
            ArrayList dataPenduduk = p.selectAll();
            for (int i = 0; i < dataPenduduk.size(); i++) {
                Object obj[] = (Object[])dataPenduduk.get(i);
                String displayStatusKtp = "";
                if (obj[9].equals("0")) {
                    displayStatusKtp = "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\"></span> Domisili</span>";
                }
                else {
                    displayStatusKtp = "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\"></span> KTP GPR</span>";
                }
                %>
                <tr>
                    <td><%= (i + 1) %></td>
                    <td><%= obj[4] %></td>
                    <td><%= obj[1] %></td>
                    <td><%= obj[7] %></td>
                    <td><%= displayStatusKtp %></td>
                    <td>
                        <%
                        String act = path + "/sites/penduduk/?action=detail&id_penduduk=" + obj[0];
                        String remove = path + "/sites/penduduk/?action=hapus&id_penduduk=" + obj[0];
                        %>
                        <a class="btn btn-primary" href="<%= act %>" title="Lihat detail"><span class="glyphicon glyphicon-tasks"></span></a>
                        <a class="btn btn-danger" href="<%= remove %>" title="hapus data penduduk!!!"><span class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
            <%
            }
            %>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
