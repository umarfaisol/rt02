package umar.faisol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Pembayaran {
    private String sqlitedb;
    private HttpServletResponse response;
    private HttpServletRequest request;

    public Pembayaran(String sqlitedb, HttpServletRequest request, HttpServletResponse response) {
        this.sqlitedb = sqlitedb;
        this.request = request;
        this.response = response;
    }

    /* digunakan untuk menyimpan tambah data */
    public String simpanTambah() {
        String returnValue = "";

        /**
        * tangkap parameter
        */
        String id_pembayaran = UUID.randomUUID().toString();
        String id_penduduk = request.getParameter("id_penduduk");
        String tahun = request.getParameter("tahun");
        String bulan = request.getParameter("bulan");
        String nominal = request.getParameter("nominal");
        Date waktu_bayar = new Date();
        String id_jenis_bayar = request.getParameter("id_jenis_bayar");

        String sql = "" +
            "INSERT INTO pembayaran(" +
                "id_pembayaran, " +
                "id_penduduk, " +
                "tahun, " +
                "bulan, " +
                "nominal, " +
                "waktu_bayar, " +
                "id_jenis_bayar " +
            ") VALUES(?, ?, ?, ?, ?, ?, ?)";

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            */
            Connection con = new SqliteCon(sqlitedb).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, id_pembayaran);
            pst.setString(2, id_penduduk);
            pst.setInt(3, new Integer(tahun).intValue());
            pst.setInt(4, new Integer(bulan).intValue());
            pst.setInt(5, new Integer(nominal).intValue());
            pst.setTimestamp(6, new java.sql.Timestamp(waktu_bayar.getTime()));
            pst.setString(7, id_jenis_bayar);
            pst.executeUpdate();

            System.out.println("Data telah disimpan!!!");
            returnValue = "ok";

            con.close();
        }
        catch (Exception e) {
            returnValue = "Error simpanTambah(): " + e;
            System.out.println("Error simpanTambah(): " + e);
        }

        return returnValue;
    }

    /* digunakan untuk menghapus data */
    public String hapus() {
        String returnValue = "";

        /**
        * tangkap parameter
        */
        String id_pembayaran = request.getParameter("id_pembayaran");
        String sql = "DELETE FROM pembayaran WHERE (id_pembayaran=?);";

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            */
            Connection con = new SqliteCon(sqlitedb).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, id_pembayaran);
            pst.executeUpdate();

            System.out.println("Data telah dihapus!!!");
            returnValue = "ok";

            con.close();
        }
        catch (Exception e) {
            returnValue = "Error hapus(): " + e;
            System.out.println("Error hapus(): " + e);
        }

        return returnValue;
    }

    /* digunakan untuk menampilkan semua data */
    public ArrayList selectAll() {
        String sql = "" +
            "SELECT " +
                "pem.id_pembayaran, " +
                "pem.tahun, " +
                "pem.bulan, " +
                "pem.nominal, " +
                "pem.waktu_bayar, " +
                "pen.nik, " +
                "pen.nama_lengkap, " +
                "pen.blok " +
            "FROM " +
                "pembayaran pem, " +
                "penduduk pen " +
            "WHERE " +
                "(pem.id_penduduk=pen.id_penduduk) ";
        String where = "";
        String order = " ORDER BY pem.waktu_bayar ASC;";
        ArrayList dataPembayaran = new ArrayList();

        sql = sql + where + order;

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            */
            Connection con = new SqliteCon(sqlitedb).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Object obj[] = new Object[8];
                obj[0] = rs.getString("id_pembayaran");
                obj[1] = rs.getInt("tahun");
                obj[2] = rs.getInt("bulan");
                obj[3] = rs.getInt("nominal");
                obj[4] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(rs.getDate("waktu_bayar").getTime()));
                obj[5] = rs.getString("nik");
                obj[6] = rs.getString("nama_lengkap");
                obj[7] = rs.getString("blok");

                dataPembayaran.add(obj);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error selectAll(): " + e);
        }
        return dataPembayaran;
    }

    /* digunakan mencari data penduduk berdasarkan id_penduduk */
    public ArrayList cariDenganId() {
        String id_pembayaran = request.getParameter("id_pembayaran");
        String sql = "" +
            "SELECT " +
                "pem.id_pembayaran, " +
                "pem.tahun, " +
                "pem.bulan, " +
                "pem.nominal, " +
                "pem.waktu_bayar, " +
                "pen.nik, " +
                "pen.nama_lengkap, " +
                "pen.blok " +
            "FROM " +
                "pembayaran pem, " +
                "penduduk pen " +
            "WHERE " +
                "(pem.id_penduduk=pen.id_penduduk) ";
        String where = "AND (pem.id_pembayaran=?)";
        String order = " ORDER BY pem.waktu_bayar ASC;";
        ArrayList dataPembayaran = new ArrayList();
        sql = sql + where + order;

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            */
            Connection con = new SqliteCon(sqlitedb).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, id_pembayaran);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                Object obj[] = new Object[8];
                obj[0] = rs.getString("id_pembayaran");
                obj[1] = rs.getInt("tahun");
                obj[2] = rs.getInt("bulan");
                obj[3] = rs.getInt("nominal");
                obj[4] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(rs.getDate("waktu_bayar").getTime()));
                obj[5] = rs.getString("nik");
                obj[6] = rs.getString("nama_lengkat");
                obj[7] = rs.getString("blok");

                dataPembayaran.add(obj);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error cariDenganId(): " + e);
        }
        return dataPembayaran;
    }

    /* digunakan untuk melihat status bayar iuran wajib */
    public boolean getSudahBayarIuran(String id_penduduk, int tahun, int bulan) {
        String sql = "" +
            "SELECT " +
                "pem.id_pembayaran " +
            "FROM " +
                "pembayaran pem " +
            "WHERE " +
                "(pem.id_penduduk=?) AND " +
                "(pem.tahun=?) AND " +
                "(pem.bulan=?) ";

        boolean retVal = false;

        try {
            Connection con = new SqliteCon(sqlitedb).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, id_penduduk);
            pst.setInt(2, tahun);
            pst.setInt(3, bulan);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                retVal = true;
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error getSudahBayarIuran(): " + e);
        }
        return retVal;
    }
}
