package umar.faisol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;

public class AccessCon {
    private String db = "";
    public AccessCon(String db) {
        this.db = db;
    }
    public Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver").newInstance();
            con = DriverManager.getConnection("jdbc:ucanaccess://" + db);
            System.out.println("[INFO] - " + new Date() + " - Koneksi Sukses ...");
        }
        catch (Exception e) {
            System.out.println("Error koneksi: " + e);
        }

        return con;
    }
}
