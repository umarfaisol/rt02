package umar.faisol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;

public class SqliteCon {
    private String sqlitedb = "";
    public SqliteCon(String sqlitedb) {
        this.sqlitedb = sqlitedb;
    }
    public Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.sqlite.JDBC").newInstance();
            con = DriverManager.getConnection("jdbc:sqlite:" + sqlitedb);
            System.out.println("[INFO] - " + new Date() + " - Koneksi Sqlite Sukses ...");
        }
        catch (Exception e) {
            System.out.println("Error koneksi: " + e);
        }

        return con;
    }
}
