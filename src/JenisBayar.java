package umar.faisol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JenisBayar {
    private String db;
    private HttpServletResponse response;
    private HttpServletRequest request;

    public JenisBayar(String db, HttpServletRequest request, HttpServletResponse response) {
        this.db = db;
        this.request = request;
        this.response = response;
    }

    /* digunakan untuk menyimpan tambah data */
    public String simpanTambah() {
        String returnValue = "";

        /**
        * tangkap parameter
        */
        String id_jenis_bayar = UUID.randomUUID().toString();
        String nama_jenis = request.getParameter("nama_jenis");

        String sql = "" +
            "INSERT INTO jenis_bayar(" +
                "id_jenis_bayar, " +
                "nama_jenis " +
            ") VALUES(?, ?)";

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            */
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, id_jenis_bayar);
            pst.setString(2, nama_jenis);
            pst.executeUpdate();

            System.out.println("Data telah disimpan!!!");
            returnValue = "ok";

            con.close();
        }
        catch (Exception e) {
            returnValue = "Error simpanTambah(): " + e;
            System.out.println("Error simpanTambah(): " + e);
        }

        return returnValue;
    }

    /* digunakan untuk menghapus data */
    public String hapus() {
        String returnValue = "";

        /**
        * tangkap parameter
        */
        String id_jenis_bayar = request.getParameter("id_jenis_bayar");

        String sql = "DELETE FROM jenis_bayar WHERE (id_jenis_bayar=?);";

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            */
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, id_jenis_bayar);
            pst.executeUpdate();

            System.out.println("Data telah dihapus!!!");
            returnValue = "ok";

            con.close();
        }
        catch (Exception e) {
            returnValue = "Error hapus(): " + e;
            System.out.println("Error hapus(): " + e);
        }

        return returnValue;
    }

    /* digunakan untuk menampilkan semua data */
    public ArrayList selectAll() {
        String sql = "SELECT * FROM jenis_bayar WHERE (1=1) ";
        String where = "";
        String order = " ORDER BY nama_jenis ASC;";
        ArrayList dataJenis = new ArrayList();

        sql = sql + where + order;

        try {
            /*Connection con = new AccessCon(db).getConnection();*/
            /*PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);*/
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Object obj[] = new Object[2];
                obj[0] = rs.getString("id_jenis_bayar");
                obj[1] = rs.getString("nama_jenis");

                dataJenis.add(obj);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error selectAll(): " + e);
        }
        return dataJenis;
    }
}
