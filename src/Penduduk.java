package umar.faisol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Penduduk {
    private String db;
    private HttpServletResponse response;
    private HttpServletRequest request;

    public Penduduk(String db, HttpServletRequest request, HttpServletResponse response) {
        this.db = db;
        this.request = request;
        this.response = response;
    }

    /* digunakan untuk menyimpan tambah data */
    public String simpanTambah() {
        String returnValue = "";

        /**
        * tangkap parameter
        */
        String id_penduduk = UUID.randomUUID().toString();
        String nik = request.getParameter("nik");
        String nama_lengkap = request.getParameter("nama_lengkap");
        String blok = request.getParameter("blok");
        String no_kk = request.getParameter("no_kk");
        String tempat_lahir = request.getParameter("tempat_lahir");
        String tanggal_lahir = request.getParameter("tanggal_lahir");
        String a_kk = request.getParameter("a_kk");
        String alamat_ktp = request.getParameter("alamat_ktp");
        String status_ktp = request.getParameter("status_ktp");

        String sql = "" +
            "INSERT INTO penduduk(" +
                "id_penduduk, " +
                "nama_lengkap, " +
                "tempat_lahir, " +
                "tanggal_lahir, " +
                "nik, " +
                "no_kk, " +
                "a_kk, " +
                "blok, " +
                "alamat_ktp, " +
                "status_ktp " +
            ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            /*Connection con = new AccessCon(db).getConnection();*/
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pst.setString(1, id_penduduk);
            pst.setString(2, nama_lengkap);
            pst.setString(3, tempat_lahir);
            pst.setDate(4, new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(tanggal_lahir).getTime()));
            pst.setString(5, nik);
            pst.setString(6, no_kk);
            pst.setInt(7, Integer.valueOf(a_kk).intValue());
            pst.setString(8, blok);
            pst.setString(9, alamat_ktp);
            pst.setInt(10, Integer.valueOf(status_ktp).intValue());
            pst.executeUpdate();

            System.out.println("Data telah disimpan!!!");
            returnValue = "ok";

            con.close();
        }
        catch (Exception e) {
            returnValue = "Error simpanTambah(): " + e;
            System.out.println("Error simpanTambah(): " + e);
        }
        /*returnValue = nik + ";" + nama_lengkap + ";" + blok;*/

        return returnValue;
    }

    /* digunakan untuk menghapus data */
    public String hapus() {
        String returnValue = "";

        /**
        * tangkap parameter
        */
        String id_penduduk = request.getParameter("id_penduduk");

        String sql = "DELETE FROM penduduk WHERE (id_penduduk=?);";

        try {
            /*Connection con = new AccessCon(db).getConnection();*/
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pst.setString(1, id_penduduk);
            pst.executeUpdate();

            System.out.println("Data telah dihapus!!!");
            returnValue = "ok";

            con.close();
        }
        catch (Exception e) {
            returnValue = "Error hapus(): " + e;
            System.out.println("Error hapus(): " + e);
        }

        return returnValue;
    }

    /* digunakan untuk menampilkan semua data */
    public ArrayList selectAll() {
        String sql = "SELECT * FROM penduduk WHERE (1=1) ";
        String where = "";
        String order = " ORDER BY blok ASC;";
        ArrayList dataPenduduk = new ArrayList();
        String status_ktp = (request.getParameter("status_ktp") == null ? "" : request.getParameter("status_ktp"));
        if (!status_ktp.equals("")) {
            where = "AND (status_ktp=?)";
        }

        /**
        * 2 --> semua
        */
        if (status_ktp.equals("2")) {
            where = "";
        }
        sql = sql + where + order;

        try {
            /*Connection con = new AccessCon(db).getConnection();*/
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if (!where.equals("")) {
                pst.setInt(1, Integer.valueOf(status_ktp).intValue());
            }
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Object obj[] = new Object[10];
                obj[0] = rs.getString("id_penduduk");
                obj[1] = rs.getString("nama_lengkap");
                obj[2] = rs.getString("tempat_lahir");
                obj[3] = new SimpleDateFormat("yyyy-MM-dd").format(new Date(rs.getDate("tanggal_lahir").getTime()));
                obj[4] = rs.getString("nik");
                obj[5] = rs.getString("no_kk");
                obj[6] = rs.getString("a_kk");
                obj[7] = rs.getString("blok");
                obj[8] = rs.getString("alamat_ktp");
                obj[9] = rs.getString("status_ktp");

                dataPenduduk.add(obj);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error selectAll(): " + e);
        }
        return dataPenduduk;
    }

    /* digunakan mencari data penduduk berdasarkan id_penduduk */
    public ArrayList cariDenganId() {
        String id_penduduk = request.getParameter("id_penduduk");
        String sql = "SELECT * FROM penduduk WHERE (1=1) AND (id_penduduk=?);";
        ArrayList dataPenduduk = new ArrayList();

        try {
            /*Connection con = new AccessCon(db).getConnection();*/
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pst.setString(1, id_penduduk);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                Object obj[] = new Object[10];
                obj[0] = rs.getString("id_penduduk");
                obj[1] = rs.getString("nama_lengkap");
                obj[2] = rs.getString("tempat_lahir");
                obj[3] = new SimpleDateFormat("yyyy-MM-dd").format(new Date(rs.getDate("tanggal_lahir").getTime()));
                obj[4] = rs.getString("nik");
                obj[5] = rs.getString("no_kk");
                obj[6] = rs.getString("a_kk");
                obj[7] = rs.getString("blok");
                obj[8] = rs.getString("alamat_ktp");
                obj[9] = rs.getString("status_ktp");

                dataPenduduk.add(obj);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error cariDenganId(): " + e);
        }
        return dataPenduduk;
    }

    /* digunakan mencari data penduduk berdasarkan NIK/nama */
    public ArrayList cariDenganNikAtauNama(String cari) {
        String sql = "SELECT * FROM penduduk WHERE LOWER(nama_lengkap LIKE ?) OR LOWER(nik) LIKE ?;";
        ArrayList dataPenduduk = new ArrayList();

        try {
            /*Connection con = new AccessCon(db).getConnection();
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            */
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, "%" + cari.toLowerCase() + "%");
            pst.setString(2, "%" + cari.toLowerCase() + "%");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Object obj[] = new Object[10];
                obj[0] = rs.getString("id_penduduk");
                obj[1] = rs.getString("nama_lengkap");
                obj[2] = rs.getString("tempat_lahir");
                obj[3] = new SimpleDateFormat("yyyy-MM-dd").format(new Date(rs.getDate("tanggal_lahir").getTime()));
                obj[4] = rs.getString("nik");
                obj[5] = rs.getString("no_kk");
                obj[6] = rs.getString("a_kk");
                obj[7] = rs.getString("blok");
                obj[8] = rs.getString("alamat_ktp");
                obj[9] = rs.getString("status_ktp");

                dataPenduduk.add(obj);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error cariDenganId(): " + e);
        }
        return dataPenduduk;
    }

    /* digunakan menghitung jumlah penduduk KTP GPR */
    public int getKtpGpr() {
        String sql = "SELECT COUNT(id_penduduk) FROM penduduk WHERE (status_ktp=?);";
        int retVal = 0;

        try {
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, 1);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                retVal = rs.getInt(1);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error getKtpGpr(): " + e);
        }
        return retVal;
    }

    /* digunakan menghitung jumlah penduduk KTP domisili */
    public int getKtpDomisili() {
        String sql = "SELECT COUNT(id_penduduk) FROM penduduk WHERE (status_ktp=?);";
        int retVal = 0;

        try {
            Connection con = new SqliteCon(db).getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, 0);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                retVal = rs.getInt(1);
            }

            con.close();
        }
        catch (Exception e) {
            System.out.println("Error getKtpDomisili(): " + e);
        }
        return retVal;
    }
}
