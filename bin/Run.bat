set TMP_CLASSPATH=%CLASSPATH%

for %%i in (%0) do cd /d %%~dpi\..

set CLASSPATH=%CLASSPATH%;.\classes\;.\fonts\;
rem Add all jars....
for %%i in ("lib\*.jar") do call ".\bin\cpappend.bat" %%i
for %%i in ("lib\*.zip") do call ".\bin\cpappend.bat" %%i

set TIK_CLASSPATH=%CLASSPATH%
set CLASSPATH=%TMP_CLASSPATH%

cd classes

java -classpath "%TIK_CLASSPATH%" umar.faisol.pokja.Main %*
cd ..
cd bin
