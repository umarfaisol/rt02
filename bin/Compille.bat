@echo off
set TMP_CLASSPATH=%CLASSPATH%

for %%i in (%0) do cd /d %%~dpi\..

rem Add all jars....
for %%i in ("lib\*.jar") do call ".\bin\cpappend.bat" %%i

set MY_CLASSPATH=%CLASSPATH%
set CLASSPATH=%TMP_CLASSPATH%
cd src
@echo on
javac -classpath "%MY_CLASSPATH%" -d ..\classes *.java
cd ..
cd classes
jar -cvfM rt02.jar .
COPY rt02.jar "../build/web/WEB-INF/lib/rt02.jar"
cd ..
cd bin
