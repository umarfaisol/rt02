# add the libraries to the IREPORT_CLASSPATH.
# EXEDIR is the directory where this executable is.
# EXEDIR=${0%/*}
DIRLIBS=../lib/*.jar
for i in ${DIRLIBS}
do
  if [ -z "$TIK_CLASSPATH" ] ; then
    TIK_CLASSPATH=$i
  else
    TIK_CLASSPATH="$i":$TIK_CLASSPATH
  fi
done

TIK_CLASSPATH="../classes":$TIK_CLASSPATH
cd ..
TIK_HOME=$(pwd)
cd classes
#echo $IREPORT_HOME

java -classpath "$TIK_CLASSPATH:$CLASSPATH" umar.faisol.pokja.Main
